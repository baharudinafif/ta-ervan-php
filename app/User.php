<?php

namespace App;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Carbon\Carbon;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'imei', 'mac', 'pin', 'token' ];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public static function adduser($imei,$pin){
		try{
			$item=User::where('imei',$imei)->first();
			if($item == null){
				$item = new User();
				$item->imei = $imei;
				$item->pin =$pin;
				$item->save();
			}
		}catch(Exception $e){
			dd($e);
		}
		return $item;
	}

  public static function getlocation($pin){
		$current = new Carbon();
		$current->timezone	= 'Asia/Jakarta';
		
		$current->hour 			= 0;
		$current->minute 		= 0;
		$current->second		= 0;
		
		$current->timezone 	= 'UTC';
		$timestamp = $current->toIso8601String();
		$list = User::where('pin', $pin)->where('updated_at', '>', $timestamp)->get();
		$list = collect($list)->map(function($item, $key) {
			$temp		= Carbon::parse($item->updated_at);
			$temp->timezone('Asia/Jakarta');
			$item 	= collect($item);
			$item['hour'] = $temp->hour + 1; 
			return $item;
		})->groupBy('hour')->map(function($arr){
			$temp = $arr->filter(function($item){ 
				return strlen($item['latitude']);
			});
			$r = rand(0, $temp->count() - 1);
			$temp = $temp->values();
			return $temp[$r]->toArray();
		})->values();
    return $list;
  }

  public static function savelocation($pin, $lat, $long){
		$item			= User::where('pin',$pin)->first();
		$new_item	= new User;
		$new_item->pin				= $item->pin;
		$new_item->imei				= $item->imei;
		$new_item->latitude 	= $lat;
    $new_item->longitude	= $long;
		$new_item->save();

    return $new_item;
  }

  public static function setToken($pin, $token){
    $item	= User::where('pin',$pin)->first();
    $item->token	= $token;
    $item->save();
  }
}
