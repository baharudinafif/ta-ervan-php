<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserController extends Controller
{
	public function show($id){
			return view('user.profile', ['user' => User::findOrFail($id)]);
	}

	public function daftar(Request $req){

		//dd($req->all());
		$output['task']			= "Add New User";
		$output['status']		= "500";
		$output['message']	= "error";

		$pin	= $this->gen_uid(6);
		$imei	= $req->input("imei");

		$item = User::adduser($imei, $pin);

		if($item != null){
			$output['status']		= "200";
			$output['message']	= "success";
			$output['data']			= $item->pin;
		}else{
			$output['status']		= "400";
			$output['message']	= "Error";
			$output['data']			= $imei;
		}
		return response()->json($output);
	}

	public function lokasi(Request $req){
		$output['task']			= "Add Location";
		$output['status']		= "500";
		$output['message']	= "error";

		$pin				= $req->input("pin");
		$latitude		= $req->input("latitude");
		$longitude	= $req->input("longitude");

		$item 			= User::savelocation($pin, $latitude, $longitude);

		$output['status']		= "200";
		$output['message']	= $item;

		return response()->json($output);
	}

	public function token(Request $req){
		$output['task']="Set Token";
		$output['status']="500";
		$output['message']="error";

		$token = $req->input("token");
		$pin = $req->input("pin");

		$item = User::setToken($pin, $token);

		$output["status"] = "200";
		$output["message"] = $item;
		return response()->json($output);
	}
	
	public function getPos(Request $req, $pin){
		$output['task']			= "Get Location";
		$output['status']		= "500";
		$output['message']	= "error";
		$arr = User::getlocation($pin);

		$output['status']		= "200";
		$output['message']	= 'success';
		$output['data'] 		= $arr;

		return response()->json($output);
	}
	
	public function fcm($token){
		$fcmEndPoint= 'https://fcm.googleapis.com/fcm/send';
		$serverKey = "key=AAAAOGDpFjw:APA91bH-eNMS7r3SsI91z84jv78DekVOwwbGmlaY_BowqbGoSx_0Gvi48cAyvkgnka3l4r0W4yZa6hf4fEaqllnVsvAcn1_qkrI6wVZTnRW_TrdxRbYR94_Gdg3VxEjoa8yIaFCt-VT1";

		$client = new Client([ 'base_uri' => $fcmEndPoint ]);

		$response = $client->request('POST', $fcmEndPoint, [
			'headers' => [ 'Authorization' => $serverKey ],
			'json' => [
				'to' => $token,
				'options' => [ 'priority' => 'high' ],
				'notification' => [
					'title' => 'Vanvan Tracker',
					'body'  => 'Lokasi Anda sedang dilacak',
					'sound' => 'default'
				]
			]
		]);
		$statusCode = $response->getStatusCode();
		return $statusCode;
	}

	public function gen_uid($l=10){
		return substr(str_shuffle("123456789ABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, $l);
	}
}
